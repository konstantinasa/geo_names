﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geonames_forms
{
    public class SharedClass
    {
        
        public String CountDistance(String con_1, String con_2, String city_1, String city_2)
        {
            if (city_1.Length == 0) return "Enter fitst city";
            if (city_2.Length == 0) return "Enter second city";

            //1st coordinates
            var xml = System.Xml.Linq.XElement.Load("http://api.geonames.org/search?name_startsWith=" + city_1 + "&country=" + con_1 + "&maxRows=1&username=lacoste1");

            var locations = xml.Descendants("geoname").Select(g => new {
                Name = g.Element("name").Value,
                Country = g.Element("countryName").Value,
                Lat = g.Element("lat").Value,
                Long = g.Element("lng").Value
            });

            if (locations.Count() == 0) return "No info regarding first country";

            //2nd coordinates
            var xml2 = System.Xml.Linq.XElement.Load("http://api.geonames.org/search?name_startsWith=" + city_2 + "&country=" + con_2 + "&maxRows=1&username=lacoste1");

            var locations2 = xml2.Descendants("geoname").Select(g => new {
                Name = g.Element("name").Value,
                Country = g.Element("countryName").Value,
                Lat = g.Element("lat").Value,
                Long = g.Element("lng").Value
            });

            if (locations2.Count() == 0) return "No info regarding second country.";

            float latt1 = float.Parse(locations.ElementAt(0).Lat);
            float longg1 = float.Parse(locations.ElementAt(0).Long);
            String cit_1 = locations.ElementAt(0).Name;
            String co_1 = locations.ElementAt(0).Country;

            float latt2 = float.Parse(locations2.ElementAt(0).Lat);
            float longg2 = float.Parse(locations2.ElementAt(0).Long);
            String cit_2 = locations2.ElementAt(0).Name;
            String co_2 = locations2.ElementAt(0).Country;

            double dist = 6371 * Math.Acos(Math.Cos(Math.PI / 180 * (90 - latt1)) * Math.Cos(Math.PI / 180 * (90 - latt2)) + Math.Sin(Math.PI / 180 * (90 - latt1)) * Math.Sin(Math.PI / 180 * (90 - latt2)) * Math.Cos(Math.PI / 180 * (longg1 - longg2)));
            return cit_1 + ", " + co_1 + " to " + cit_2 + ", " + co_2 + "\r\nDistance is: " + Math.Round(dist, 3).ToString() + " KM.";

        }


        public String[] GetInfo(string coun, String place)
        {

            String[] all_data;
            all_data = new string[10];
            //all_data[0] = "test";
            //all_data[1] = "test";
            //all_data[2] = "test";
            //all_data[3] = "test";
            //return all_data;

            if (place.Length == 0)
            {
                all_data[0] = "Enter any place";
                return all_data;
            }

            var xml = System.Xml.Linq.XElement.Load("http://api.geonames.org/search?name_startsWith=" + place + "&country=" + coun + "&maxRows=1&style=LONG&username=lacoste1");

            var locations = xml.Descendants("geoname").Select(g => new {

                Name = g.Element("name").Value,
                Country = g.Element("countryName").Value,
                Lat = g.Element("lat").Value,
                Long = g.Element("lng").Value,
                countryCode = g.Element("countryCode").Value,
                fcodeName = g.Element("fcodeName").Value,
                population = g.Element("population").Value

            });

            if (locations.Count() == 0)
            {
                all_data[0] = "No info regarding this place";
                return all_data;
            }

            all_data[0] = locations.ElementAt(0).Name;
            all_data[1] = locations.ElementAt(0).Country;
            all_data[2] = locations.ElementAt(0).Lat;
            all_data[3] = locations.ElementAt(0).Long;
            all_data[4] = locations.ElementAt(0).population;
            all_data[5] = locations.ElementAt(0).fcodeName;
            all_data[6] = locations.ElementAt(0).countryCode;

            return all_data;
        }


    }
}
