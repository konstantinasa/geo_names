﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace geonames_forms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.Text = "Geonames Client";

            // no smaller than design time size
            this.MinimumSize = new System.Drawing.Size(this.Width, this.Height);
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            this.tabControl1.Selected += (s, ee) => {
                TabControl tab = s as TabControl;
                switch (tab.SelectedTab.Name)
                {
                    case "tabPage1":
                        this.AcceptButton = this.button1;
                        break;
                    case "tabPage2":
                        this.AcceptButton = this.button2;
                        break;
                    default:
                        this.AcceptButton = this.button2;
                        break;
                }
            };

            //this.textBox1.Focus(); // set control focus 
            //this.textBox1.Enter += new EventHandler(textBox1_Enter); // enter event==get focus 
            //this.textBox1.Leave += new EventHandler(textBox1_lostFocus);
            //this.listBox2.Focus();
 


            //this.textBox2.Focus(); // set control focus 
            //this.textBox2.Enter += new EventHandler(textBox2_Enter); // enter event==get focus 
            //this.textBox2.Leave += new EventHandler(textBox2_lostFocus);

        }

        //protected void textBox1_Enter(Object sender, EventArgs e)
        //{
        //    if (textBox1.Focused || listBox2.Focused){
        //        listBox2.Visible = true;
        //    }
                
            
        //}

        //protected void textBox1_lostFocus(Object sender, EventArgs e)
        //{
        //    listBox2.Visible = false;
        //}

        //protected void textBox2_Enter(Object sender, EventArgs e)
        //{
        //    listBox3.Visible = true;

        //}

        //protected void textBox2_lostFocus(Object sender, EventArgs e)
        //{
        //    listBox3.Visible = false;
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            String country_1 = comboBox1.Text;
            String country_2 = comboBox2.Text;

            String city_1 = textBox1.Text;
            String city_2 = textBox2.Text;

            //------------------------------------

            

            //------------------------------------
            var shared = new SharedClass();
            label26.Text = shared.CountDistance(country_1, country_2, city_1, city_2);

            //------------------------------------


        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            String country = comboBox3.Text;
            String city = textBox3.Text;

            listBox1.Items.Clear();
            textBox4.Text = "";

            if (city.Length == 0)
            {
                label16.Text = "Enter any place";
            }

            var xml = System.Xml.Linq.XElement.Load("http://api.geonames.org/search?name_startsWith=" + city + "&country=" + country + "&maxRows=200&style=LONG&username=lacoste1");

            var locations = xml.Descendants("geoname").Select(g => new {

                Name = g.Element("name").Value,
                Country = g.Element("countryName").Value,
                Lat = g.Element("lat").Value,
                Long = g.Element("lng").Value,
                countryCode = g.Element("countryCode").Value,
                fcodeName = g.Element("fcodeName").Value,
                population = g.Element("population").Value

            });

            if (locations.Count() == 0)
            {
                label16.Text = "No info regarding this place";
            }

            listBox1.Visible = true;
            label23.Visible = true;
            textBox4.Visible = true;
            button5.Visible = true;

            foreach (var location in locations)
            {
                listBox1.Items.Add(location.Name);
            }

            textBox4.Text = listBox1.Items.Count.ToString();


        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            String country = comboBox3.Text;
            String city = textBox3.Text;

            var shared = new SharedClass();

            String[] all_data;
            all_data = new string[10];
            all_data = shared.GetInfo(country, city);

            Process.Start("http://www.google.com/maps/place/" + all_data[2] + "," + all_data[3]);
        }

        private void button4_Click(object sender, EventArgs e)
        {

            String city_1 = textBox1.Text;
            String city_2 = textBox2.Text;

            Process.Start("https://www.google.lt/maps/dir/" + city_1 + "/" + city_2);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            var shared = new SharedClass();

            string[] all_data;
            all_data = new string[10];
            all_data = shared.GetInfo(comboBox3.Text, listBox1.SelectedItem.ToString());

            label16.Text = all_data[0];
            label17.Text = all_data[1];
            label18.Text = all_data[2];
            label19.Text = all_data[3];
            label20.Text = all_data[4];
            label21.Text = all_data[5];
            label22.Text = all_data[6];

        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox1.Visible = false;
            label23.Visible = false;
            textBox4.Visible = false;
            button5.Visible = false;

            textBox4.Text = "";
            label16.Text = "";
            label17.Text = "";
            label18.Text = "";
            label19.Text = "";
            label20.Text = "";
            label21.Text = "";
            label22.Text = "";

            comboBox3.Text = "";
            textBox3.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            listBox2.Visible = true;
            label24.Visible = true;
            textBox5.Visible = true;

            listBox2.Items.Clear();

            String city_1 = textBox1.Text;
            String country_1 = comboBox1.Text;
            
            var xml = System.Xml.Linq.XElement.Load("http://api.geonames.org/search?name_startsWith=" + city_1 + "&country=" + country_1 + "&maxRows=30&style=LONG&username=lacoste1");

            var locations = xml.Descendants("geoname").Select(g => new {

                Name = g.Element("name").Value,
                Country = g.Element("countryName").Value,
                Lat = g.Element("lat").Value,
                Long = g.Element("lng").Value,
                countryCode = g.Element("countryCode").Value,
                fcodeName = g.Element("fcodeName").Value,
                population = g.Element("population").Value

            });

            if (locations.Count() == 0)
            {
                label26.Text = "No info regarding this place";
            }


            foreach (var location in locations)
            {
                listBox2.Items.Add(location.Name);
            }

            textBox5.Text = listBox2.Items.Count.ToString();

            if (textBox1.Text.Length == 0)
            {
                label26.Text = "Enter first city";
                listBox2.Items.Clear();
                textBox5.Text = "";
            }
            else
            {
                label26.Text = "";
            }

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = listBox2.SelectedItem.ToString();
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            listBox3.Visible = true;
            label25.Visible = true;
            textBox6.Visible = true;
            listBox3.Items.Clear();

            String city_2 = textBox2.Text;
            String country_2 = comboBox2.Text;

            var xml = System.Xml.Linq.XElement.Load("http://api.geonames.org/search?name_startsWith=" + city_2 + "&country=" + country_2 + "&maxRows=30&style=LONG&username=lacoste1");

            var locations2 = xml.Descendants("geoname").Select(g => new {

                Name = g.Element("name").Value,
                Country = g.Element("countryName").Value,
                Lat = g.Element("lat").Value,
                Long = g.Element("lng").Value,
                countryCode = g.Element("countryCode").Value,
                fcodeName = g.Element("fcodeName").Value,
                population = g.Element("population").Value

            });

            if (locations2.Count() == 0)
            {
                label26.Text = "No info regarding this place";
            }


            foreach (var location in locations2)
            {
                listBox3.Items.Add(location.Name);
            }

            textBox6.Text = listBox3.Items.Count.ToString();

            if (textBox2.Text.Length == 0)
            {
                label26.Text = "Enter second city";
                listBox3.Items.Clear();
                textBox5.Text = "";
            }
            else
            {
                label26.Text = "";
            }
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox2.Text = listBox3.SelectedItem.ToString();
        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
